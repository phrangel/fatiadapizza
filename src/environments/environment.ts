// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDnu8iYOtzIjHqHxnvFEL5M46l7dGvXOJw",
    authDomain: "fatiapizza-d6cd1.firebaseapp.com",
    databaseURL: "https://fatiapizza-d6cd1.firebaseio.com",
    projectId: "fatiapizza-d6cd1",
    storageBucket: "fatiapizza-d6cd1.appspot.com",
    messagingSenderId: "1073424921719"
  }
};
